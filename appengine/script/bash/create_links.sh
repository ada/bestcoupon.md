#!/usr/bin/env bash
. ./conf.sh

sudo ln -s --no-dereference ${COMMON_LIB}/django-nonrel/django            ${XC_PATH}/appengine/django
sudo ln -s --no-dereference ${COMMON_LIB}/djangotoolbox/djangotoolbox     ${XC_PATH}/appengine/djangotoolbox
sudo ln -s --no-dereference ${COMMON_LIB}/django-autoload/autoload        ${XC_PATH}/appengine/autoload
sudo ln -s --no-dereference ${COMMON_LIB}/django-dbindexer/dbindexer      ${XC_PATH}/appengine/dbindexer
sudo ln -s --no-dereference ${COMMON_LIB}/djangoappengine/djangoappengine ${XC_PATH}/appengine/djangoappengine





