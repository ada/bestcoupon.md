#!/usr/bin/env bash
. ./conf.sh

sudo rm ${XC_PATH}/appengine/django
sudo rm ${XC_PATH}/appengine/djangotoolbox
sudo rm ${XC_PATH}/appengine/autoload
sudo rm ${XC_PATH}/appengine/dbindexer
sudo rm ${XC_PATH}/appengine/djangoappengine
