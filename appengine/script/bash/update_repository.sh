#!/usr/bin/env bash

# http://www.allbuttonspressed.com/projects/djangoappengine

ROOT_PATH=/code4

set -x
cd ${ROOT_PATH}/lib/django-nonrel 
#git checkout .
git pull

cd ${ROOT_PATH}/lib/djangoappengine
#git checkout .
git pull

cd ${ROOT_PATH}/lib/djangotoolbox
#git checkout .
git pull

cd ${ROOT_PATH}/lib/django-autoload
#hg update ?????????
hg pull

cd ${ROOT_PATH}/lib/django-dbindexer
#git checkout .
git pull

cd ${ROOT_PATH}/lib/django-testapp
#git checkout .
git pull
