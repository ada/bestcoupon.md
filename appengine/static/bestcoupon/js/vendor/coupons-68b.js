var $j = jQuery;
var APP_COUPONSINC = {}



APP_COUPONSINC.layout = (function (g) {
    var b,
        f = g(window),
        t = g(document),
        l = g("body"),
        d = g("#main"),
        m = g("mod-intronagbar"),
        k = g(".mod-stalker"),
        i = k.next(),
        r = k.offset().top,
        o = false,
        n = 0;

    function p(u) {
        if (u === "fixed") {
            l.addClass("fixed-header");
            var v = g("#main").offset().left + (d.outerWidth() - k.outerWidth()) / 2;
            k.css("left", v)
        } else {
            l.removeClass("fixed-header");
            k.css("left", "auto")
        }
    }

    function e(u) {
        var v;
        if (u === "fixed") {
            v = k.outerHeight(true);
            i.css("margin-top", v + "px")
        } else {
            i.css("margin-top", "auto")
        }
    }

    function s() {
        if (l.hasClass("fixed-header")) {
            p("fixed")
        } else {
            p("static")
        }
    }

    function j() {
        var u = f.scrollTop();
        if (u > r) {
            p("fixed");
            e("fixed")
        } else {
            p("static");
            e("static")
        }
        n = u
    }

    function h(u, v) {
        r = k.offset().top + m.outerHeight(true)
    }

    function c(u) {
        o = true;
        j()
    }

    function q() {
        f.scroll(c);
        f.resize(s);
        t.bind("couponsinc:intronagbar", h)
    }

    function a(v) {
        b = v || this;
        var u = 112;
        q();
        setInterval(function () {
            if (o) {
                t.trigger("couponsinc:windowScrolled");
                o = false
            }
        }, u)
    }

    return{processScroll: j, onReady: a}
}(jQuery));
jQuery(document).ready(function () {
    APP_COUPONSINC.layout.onReady()
});






