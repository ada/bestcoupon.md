(function() {
    var portamento = (function() {
        var self = this
        var TO_CLIP = $("#hidden-container-to-clip").html()
        var UNDO_CLIP = $("#hidden-container-undo-clip").html()
        self.select = {
            categoriesItems     : "#coupon-categories li[id!='label_show_all']",
            categoriesContainer : ".mod-categories",
            showLess            : "a.show-less",
            showMore            : "a.show-more",
            couponCounter       : "#c-counter",
            couponCounterLabel  : "#c-counter-click",
            unclipAllCoupons    : "#c-unclip-all",
            couponCell          : ".coupon",
            couponCellClipped   : ".coupon.clipped",
            couponUnclip        : ".coupon .unclip-action",
            couponUnclipAll     : "#c-unclip-all",
            couponShareBox      : ".coupon .share-box",
            formPippedIds       : "#pipped-ids",
            formPrintButton     : "#final-print-button",
            printCheckbox       : ".print-checkbox",
            printUnchecked      : "li.coupon-print.unchecked",
            printChecked        : "li.coupon-print:not(.unchecked)",
            loadMoreCoupons     : "#load-more-coupons"
        }

        self._compareNumbers = function ( a, b){
            return((a > b) - (a < b))
        }

        self._setTopForList = function () {
            var to_show = []
            var count = 0

            $(self.select.categoriesItems).each(function(index, v) {
                v = parseInt($(this).attr("data-count"))
                to_show.push(v)
            })

            // get first 3 element form sort descendant
            to_show = to_show.sort(self._compareNumbers).reverse().slice(0, 3)

            // apply style for matches
            $(self.select.categoriesItems).each(function(index, v) {
                v = parseInt($(this).attr("data-count"))
                if (to_show.indexOf(v) !== -1 && count < 3) {
                    $(this).addClass("top")
                    count += 1
                }
            })
        }

        self.collapse = function(){
            $(self.select.categoriesContainer).removeClass("show-less").addClass("show-more")
            self._setTopForList()
        }

        self.expand = function(){
            $(self.select.categoriesContainer).removeClass("show-more").addClass("show-less")
        }

        self.couponCounterAdd = function(v){
            var counter= parseInt($(self.select.couponCounterLabel).text());
            counter += v
            $(self.select.couponCounterLabel).text(counter)

            if (counter === 0) {
                $(self.select.couponCounter).addClass("clipped-none")
                $(self.select.unclipAllCoupons).addClass("clipped-none")
            } else {
                $(self.select.couponCounter).removeClass("clipped-none")
                $(self.select.unclipAllCoupons).removeClass("clipped-none")
            }
        }

        self.submitPrint = function($selected) {
            var id;
            var coupon_clipped = [];

            $selected.each(function(index, val){
                id = $(this).attr("id").replace(/^coupon-/,"")
                coupon_clipped.push(id)
            })
            coupon_clipped = coupon_clipped.join("|")
            if (coupon_clipped) {
                $(self.select.formPippedIds).attr("value", coupon_clipped);
                document.forms['print-button'].submit();
            } else {
                //
            }

        }

        var path = window.location.pathname
        if (path === "/coupons/" || path === "/coupons" || path === "/") {
            // init
            self._setTopForList()

            // add redundant html menu
            $(self.select.couponCell).
                append(TO_CLIP).
                append(UNDO_CLIP)

            // collapse menu
            $(self.select.showLess).click(self.collapse)
            // expand menu
            $(self.select.showMore).click(self.expand)

            // clip coupon
            $(self.select.couponCell).click(function(){
                $(this).addClass("clipped")
                self.couponCounterAdd(+1)
            })
            // unclip coupon
            $(self.select.couponUnclip).click(function(e){
                e.stopPropagation();
                $(this).parents(".coupon").removeClass("clipped show-share")
                self.couponCounterAdd(-1)
            })
            // unclip all
            $(self.select.couponUnclipAll).click(function(e) {
                if (confirm("Refuza toate cupoanele ?"))
                {
                    $(self.select.couponCounter).addClass("clipped-none")
                    $(self.select.couponCounterLabel).text("0")
                    $(self.select.couponUnclipAll).addClass("clipped-none")
                    $(self.select.couponCellClipped).removeClass("clipped")
                }
            })

            // show share box
            $(self.select.couponShareBox).click(function(e){
                e.stopPropagation();
                var $parent = $(this).parents(".coupon")
                $parent.addClass("show-share").mouseleave(function () {
                    $(this).removeClass("show-share").unbind("mouseleave")
                })
            })

            // print
            $(self.select.couponCounter).click(function () {
                self.submitPrint($(self.select.couponCellClipped))
            })

            // load-more-coupons
            $(self.select.loadMoreCoupons).click(function () {
                alert("Vezi mai multe cupoane (in constructie)")
            })

        } else {
            $(self.select.printCheckbox).click(function(){
                var $p = $(this).parents("li")
                if ( $p.hasClass("unchecked") ) {
                    $p.removeClass("unchecked");
                } else {
                    $p.addClass("unchecked")
                }
            })

            $(self.select.formPrintButton).click(function(){
                var $checked   = $(self.select.printChecked)
                var $unchecked = $(self.select.printUnchecked)

                if ($unchecked.size()) {
                    if($checked.size()) {
                        if (confirm("Creaza alta lista de cupoane (a fost exclus un cuppon)?")) {
                            self.submitPrint($checked)
                        }
                    } else {
                        alert("Alegeti cel putin un cuppon !")
                    }

                } else {
                    window.print()
                }
            })
        }

    })();















}());

