(function() {
// Bind the event

    var $paragraph = $(".paragraph")

    $(window).hashchange( function(){
        // Alerts every time the hash changes!


        switch(location.hash)
        {
            case "#offline":
                $paragraph.addClass("offline").removeClass("online")
                break
            default: //"#online"
                $paragraph.addClass("online").removeClass("offline")
                break
        }

    })

    // Trigger the event (useful on page load).
    $(window).hashchange();

}());

