"""
simple middlware to block IP addresses via settings variable BLOCKED_IPS
http://djangosnippets.org/snippets/744/
"""
from django.conf import settings
from django import http

class AllowedIpMiddleware(object):

    def process_request(self, request):
        if request.META['REMOTE_ADDR'] not in settings.ALLOWED_IPS:
            return http.HttpResponseForbidden('<h1>Forbidden ip %r</h1>' % request.META['REMOTE_ADDR'])

        return None

