import views

from django.conf.urls.defaults import patterns

urlpatterns = patterns(
    '',
    (r"^/?$", views.root),
    (r"^coupons/?$", views.coupons),
    (r"^support\.html/?$", views.support),
    (r"^about\.html/?$", views.about),
    (r"^terms\.html/?$", views.terms),
    (r"^privacy\.html/?$", views.privacy),
    (r"^online/?$", views.online),
    (r"^contact\.html?$", views.contact),
    (r"^print(?:/([a-z0-9]+)?)?$", views.print_),
)