from collections import OrderedDict
import datetime
import hashlib

from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.shortcuts import render_to_response

from bestcoupon.bestcoupon_db import Print
from bestcoupon.db_json import rows, categories


def _sort_categories(big_d):
    d = big_d["items"]
    od = OrderedDict()
    for k, v in sorted(d.items(), key=lambda x: x[1]["lang"]["ro"]):
        od[k] = v

    big_d["items"] = od

    return big_d


def coupons(request, menu_collapse=True):
    """
    :param request:
    :param menu_collapse: collapse menu
    :return:
    """
    global categories
    # emulate featured advert
    #rows.insert(3, None)

    categories = _sort_categories(categories)

    template = {
        "active" : "coupons",
        "title":         "Main",
        "rows":          rows,
        "categories":    categories,
        "menu_collapse": menu_collapse
    }

    return render_to_response("bestcoupon_coupons.html", template)


def root(request):
    return coupons(request, menu_collapse=False)


def support(request):
    template = {
        "active" : "support"
    }

    return render_to_response("bestcoupon_support.html", template)


def about(request):
    template = {
        "active" : "about"
    }

    return render_to_response("bestcoupon_about.html", template)


def terms(request):
    template = {
        "active" : "terms"
    }
    return render_to_response("bestcoupon_terms.html", template)


def privacy(request):
    template = {
        "active" : "privacy"
    }

    return render_to_response("bestcoupon_privacy.html", template)


def online(request):
    template = {
        "active" : "online"
    }

    return render_to_response("bestcoupon_online.html", template)

def contact(request):
    template = {
        "active" : "contact"
    }

    return render_to_response("bestcoupon_contact.html", template)


def print_(request, session=None):
    template = {
    }


    if request.method == "GET" and session:

        try:
            res = Print.objects.get(key=session)
        except ObjectDoesNotExist:
            res = None

        if res:
            clipped = map(int, res.clipped.split("|"))
            # TODO :# Get blogs entries with id 1, 4 and 7
            # >>> Blog.objects.filter(pk__in=[1,4,7])
            clipped_objects = []
            for item in rows:
                if item["id"] in clipped:
                    clipped_objects.append(item)

            template["clipped"] = clipped_objects


    elif request.method == "POST" and not session:
        pipped_ids = request.POST.get("pipped_ids")
        if not pipped_ids:
            return render_to_response("bestcoupon_print_error.html")

        clipped_hash = hashlib.sha224(pipped_ids).hexdigest()
        try:
            Print.objects.get(key=clipped_hash)
        except ObjectDoesNotExist:
            Print.objects.create(
                key=clipped_hash,
                clipped=pipped_ids,
                date=datetime.datetime.utcnow()
            )

        return redirect("/print/%s" % clipped_hash)

        # else go at the end of function


    else:
        return render_to_response("bestcoupon_print_error.html")


    return render_to_response("bestcoupon_print.html", template)