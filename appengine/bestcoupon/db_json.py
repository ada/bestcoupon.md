# -*- coding: utf-8 -*-

rows = [
        {
            "amount": {
                "value": 8.88
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 0,
            "date" : "1/03/2013"
        },
        {
            "amount": {
                "value": 1.13
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 1,
            "date" : "2/03/2013"
        },
        {
            "amount": {
                "value": 2.24
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 2,
            "date" : "3/03/2013"
        },
        {
            "amount": {
                "value": 3
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 3,
            "date" : "4/03/2013"
        },
        {
            "amount": {
                "value": 4
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 4,
            "date" : "5/03/2013"
        },
        {
            "amount": {
                "value": 5
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 5,
            "date" : "6/03/2013"
        },
        {
            "amount": {
                "value": 6
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 6,
            "date" : "7/03/2013"
        },
        {
            "amount": {
                "value": 7.77
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 7,
            "date" : "8/03/2013"
        },
        {
            "amount": {
                "value": 7.99
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 8,
            "date" : "8/03/2013"
        },
        {
            "amount": {
                "value": 9.99
            },
            "name": "Gel de dus \"Element O3\"",
            "brand": "Viorica Cosmetic",
            "details_include": "promotie valabila la min 30 lei pina la 14 feb.",
            "id" : 9,
            "date" : "8/03/2013"
        },
        {
            "amount": {
                "percent": 50
            },
            "name" : "Illusion romantique",
            "brand" : "Viorica Cosmetic",
            "details_include" : "min 2 unitati sau suma 50lei",
            "id" : 10,
            "date" : "8/03/2013"
        },
        {
            "amount": {
                "percent": 11
            },
            "name" : "Illusion romantique",
            "brand" : "Viorica Cosmetic",
            "details_include" : "min 2 unitati sau suma 50lei",
            "id" : 11,
            "date" : "9/03/2013"
        },
        {
            "amount": {
                "value": 20
            },
            "name" : "Pastrama Extra",
            "brand" : "Basarabia Nord",
            "details_include" : "min 2 kg",
            "id" : 12,
            "date" : "10/03/2013"
        },
        {
            "amount": {
                "value": 21.99
            },
            "name" : "Rulada Prestige",
            "brand" : "Nivalli",
            "details_include" : "min 1 kg",
            "id" : 13,
            "date" : "11/03/2013"
        },
        {
            "amount": {
                "percent": 15
            },
            "name" : "Parizer Doctorscaea",
            "brand" : "Rogob - Carne si mezeluri",
            "details_include" : "min 10 kg",
            "details_exclude" : "doar carne de jita",
            "id" : 14,
            "date" : "12/03/2013"
        },
        {
            "amount": {
                "value": 30.4
            },
            "name" : "Parizer Doctorscaea",
            "brand" : "Meat House",
            "details_include" : "",
            "id" : 15,
            "date" : "13/03/2013"
        },
        {
            "amount": {
                "percent": 40
            },
            "name" : "Macrou f/c sarat",
            "brand" : "Telemar",
            "details_include" : "min 30 kg",
            "id" : 16,
            "date" : "14/03/2013"
        },
        {
            "amount": {
                "percent": 7
            },
            "name" : "Fileu de somon",
            "brand" : "Ocean Fish",
            "details_include" : "pentru orice produs cumparat",
            "details_exclude" : "Reducerea nu se aplica la raci",
            "id" : 17,
            "date" : "15/03/2013"
        },
        {
            "amount": {
                "percent": 10
            },
            "name" : "Cascaval Svlivocnii",
            "brand" : "Берёзка",
            "details_include" : "min 200 gr",
            "id" : 18,
            "date" : "16/03/2013"
        },
        {
            "amount": {
                "value": 10.80
            },
            "name" : "Brinza de Olanda cu cheag tare",
            "brand" : "Alba",
            "details_include" : "min 2 unitati",
            "id" : 19,
            "date" : "17/03/2013"
        }
]


categories = {
    "total_count" : 215,
    "items" : {
        "food_and_drinks" : {
            "count" : 64,
            "id" : "1",
            "lang" : {
                "ro" : "Alimentație și Băuturi",
                "en" : "Food and Drinks",
            }
        },
        "health_and_care" : {
            "count" : 24,
            "id" : "2",
            "lang" : {
                "en" : "Health and Care",
                "ro" : "Sănatate și Frumusețe"
            }
        },
        "home_and_decor" : {
            "count" : 1,
            "id" : "3",
            "lang" : {
                "ro" : "Casa și Decorațiuni",
                "en" : "Home and Decor"
            }
        },
        "travel" : {
            "count" : 7,
            "id" : "4",
            "lang" : {
                "ro" : "Turism",
                "en" : "Travel"
            }
        },
        "apparel_and_accesories" : {
            "count" : 7,
            "id" : "4",
            "lang" : {
                "en" : "Apparel and Accesories",
                "ro" : "Vestimentație și Accesorii"
            }
        },
        "it_and_telecommunication" : {
            "count" : 1,
            "id" : "5",
            "lang" : {
                "en" : "It And Telecommunication",
                "ro" : "IT și Telecomunicații"
            }
        },
        "restaurants_recreations_and_events" : {
            "count" : 4,
            "id" : "6",
            "lang" : {
                "en" : "Restaurants Recreations and Events",
                "ro" : "Localuri și Evenimente"
            }
        },
        "automotive" : {
            "count" : 31,
            "id" : "7",
            "lang" : {
                "en" : "Automotive",
                "ro" : "Auto și Transport"
            }
        },
        "others" : {
            "count" : 7,
            "id" : "8",
            "lang" : {
                "en" : "Others",
                "ro" : "Diverse"
            }
        }
    }
}
