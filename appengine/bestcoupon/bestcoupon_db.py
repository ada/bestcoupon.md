from django.db import models


class Print(models.Model):
    key = models.CharField(primary_key=True)
    clipped = models.CharField()
    date = models.DateTimeField()
    is_valid = models.BooleanField()
    marker = models.BooleanField()